#!/bin/bash

#################
# nom : brute.sh
# auteur : nordine@valog.f
# syntaxe : pas de paramètres
# description : test plusieurs combinaisons de mot de passe pour déchifrer un répertoire home
# date : 2020-05
################

WRAPPED_PASSPHRASE="/home/.ecryptfs/$USERNAME/.ecryptfs/wrapped-passphrase"
test ! -f "${WRAPPED_PASSPHRASE}" && echo "${WRAPPED_PASSPHRASE}" does not exist. && exit 1

function unwrap() {
	echo "$@".
	echo $(( $COMPTEUR*100/$NBLI ))"%"

	printf "%s" "$@" | ecryptfs-unwrap-passphrase "${WRAPPED_PASSPHRASE}" - 2>/dev/null

	test $? = 0 && echo "Le mot de passe est $@" && exit 0
}

echo -e {4,A}m{3,E}{1,l}i{3,E}-{P,p}r{o,O,0}uv{o,O,0}{s,S,5}t"\n" > liste.txt

NBLI=`cat liste.txt|wc -l`

for PASSTEST in `cat liste.txt `; do COMPTEUR=$((COMPTEUR+1)) ; unwrap $PASSTEST ; done

exit 1
