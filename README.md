# incertitude

Si vous avez une incertitude sur un mot de passe inscrit sur un document papier.

## Note
j'utilise dans mes texte le masculin pour désigner tous les genres.

## Contexte

Un membre de linuxmaine s'est vu confier un disque dur chiffré avec un mot de passe inscrit sur un document papier.
Il n'était pas possible d'être sûr à 100% de lire correctement les caractères car il arrive de confondre certains caractères :

- l (minuscule), 1 (chiffre) 
- 4 (chiffre), A (majuscule)
- p (minuscule), P (majuscule)
- o (minuscule), O (majuscule), 0 (chiffre)

Il est possible de tester toutes les solutions manuellement mais cela risque de prendre beaucoup de temps.

l'exemple donné ici utilise le mot de passe 4m3li3-Pr0uvOst 


[![exemple de mot de passe illisible](https://framagit.org/linuxmaine/articles/incertitude/-/blob/29a3b84050275337d4b7d6f8853639c8628189ac/password.jpg =1000x200)]
